# Privacy Policy

This app uses the sensitive permissions

 - android.permission.RECORD_AUDIO
 - android.permission.READ_CONTACTS

Both these permissions are necessary for the operation of the app.
RECORD_AUDIO is obvious, since the purpose of the app is to perfrom speech
to text conversion. The READ_CONTACTS permissions is needed for performing
outgoing phone calls from the app.

We do not gather, upload, or store locally or remotely any personal data
about the user.

The audio recordings are done through the
[android.speech.SpeechRecognizer](https://developer.android.com/reference/android/speech/SpeechRecognizer)
library, which is an official Google library. See [here](https://policies.google.com/privacy) for the terms of this library.
The handling of data by this library is completely out of our control.
We have no access to the raw recordings. 
The app has access to the recognized text, but this is only used for the
purposes of opening the navigation app with the required address, and for
opening the list of matched contacts.
Our app never stores the recognized text, neither locally nor remotely, and
never uploads the text to any external services. 

The contacts are also never stored, neither locally, nor remotely, and never
uploaded to external services. They are only used for displaying the list of
matching contacts to the user, so that he can start a phone call to one of them.
