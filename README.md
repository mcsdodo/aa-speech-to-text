# AA Speech To Text

## Introduction

This app allows speech recognition functionality for languages that are not
officially supported by Android Auto.

Speech recognition can be used to:

1. Enter an address (or search term) in your local language, and then the app
   will open your navigation app so that you can navigate without typing the
   address manually.

2. Enter a contact name in your local language, and then the app will start a
   phone call to the specific contact. To enter the contact, say the word
   "contact", and then the name of the contact as stored in the address book.

## Target Languages

If you live in any of the following countries, you don't need this app, Android
Auto works natively in your language:

- Argentina, Australia, Austria, Belgium, Bolivia, Brazil, Canada, Chile,
  Colombia, Costa Rica, Denmark, Dominican Republic, Ecuador, France, Germany,
  Guatemala, India, Indonesia, Ireland, Italy, Japan, Mexico, Netherlands,
  New Zealand, Norway, Panama, Paraguay, Peru, Philippines, Poland, Portugal,
  Puerto Rico, Russia, Singapore, South Africa, South Korea, Spain, Sweden,
  Switzerland, Taiwan, Thailand, Turkey, United Kingdom, United States,
  Uruguay, Venezuela

If you use one of the following languages, in one of the following countries,
this app is for you:

- Albanian (Albania) (sq-AL), Amharic (Ethiopia) (am-ET),
  Arabic (Algeria) (ar-DZ), Arabic (Bahrain) (ar-BH), Arabic (Egypt) (ar-EG),
  Arabic (Iraq) (ar-IQ), Arabic (Israel) (ar-IL), Arabic (Jordan) (ar-JO),
  Arabic (Kuwait) (ar-KW), Arabic (Lebanon) (ar-LB), Arabic (Morocco) (ar-MA),
  Arabic (Oman) (ar-OM), Arabic (Qatar) (ar-QA), Arabic (Saudi Arabia) (ar-SA),
  Arabic (State of Palestine) (ar-PS), Arabic (Tunisia) (ar-TN),
  Arabic (United Arab Emirates) (ar-AE), Arabic (Yemen) (ar-YE),
  Armenian (Armenia) (hy-AM), Azerbaijani (Azerbaijan) (az-AZ),
  Bengali (Bangladesh) (bn-BD), Bosnian (Bosnia and Herzegovina) (bs-BA),
  Bulgarian (Bulgaria) (bg-BG), Burmese (Myanmar) (my-MM), Chinese,
  Cantonese (Traditional Hong Kong) (yue-Hant-HK), Chinese,
  Mandarin (Simplified, China) (zh (cmn-Hans-CN), Croatian (Croatia) (hr-HR),
  Czech (Czech Republic) (cs-CZ), English (Ghana) (en-GH),
  English (Hong Kong) (en-HK), English (Kenya) (en-KE),
  English (Nigeria) (en-NG), English (Pakistan) (en-PK),
  English (Tanzania) (en-TZ), Estonian (Estonia) (et-EE),
  Finnish (Finland) (fi-FI), Georgian (Georgia) (ka-GE), Greek (Greece) (el-GR),
  Hebrew (Israel) (iw-IL), Hungarian (Hungary) (hu-HU),
  Icelandic (Iceland) (is-IS), Kazakh (Kazakhstan) (kk-KZ),
  Khmer (Cambodia) (km-KH), Lao (Laos) (lo-LA), Latvian (Latvia) (lv-LV),
  Lithuanian (Lithuania) (lt-LT), Macedonian (North Macedonia) (mk-MK),
  Malay (Malaysia) (ms-MY), Mongolian (Mongolia) (mn-MN),
  Nepali (Nepal) (ne-NP), Persian (Iran) (fa-IR), Romanian (Romania) (ro-RO),
  Serbian (Serbia) (sr-RS), Sinhala (Sri Lanka) (si-LK),
  Slovak (Slovakia) (sk-SK), Slovenian (Slovenia) (sl-SI),
  Spanish (El Salvador) (es-SV), Spanish (Honduras) (es-HN),
  Spanish (Nicaragua) (es-NI), Swahili (Kenya) (sw-KE),
  Swahili (Tanzania) (sw-TZ), Tamil (Malaysia) (ta-MY),
  Tamil (Sri Lanka) (ta-LK), Ukrainian (Ukraine) (uk-UA),
  Urdu (Pakistan) (ur-PK), Uzbek (Uzbekistan) (uz-UZ),
  Vietnamese (Vietnam) (vi-V

## Permissions

For the app to work correctly, you need to allow the following permissions:

- Microphone, so that the speech recognition engine can work
- Contacts, so that app can display a list of the contacts that match the name
  you entered.
- Phone, so that app can start a phone call to the selected contact.

## User instructions

1. After installing the app, open the app **in your phone**. There you will
   select the speech recognition language, and the app locale. You will also
   be presented with popups for enabling the required permissions.

   ![Settings screen](demo_images/settings_screen.png)

2. Connect to android auto. You should see the app in the app drawer in
   android auto:

   ![App drawer](demo_images/app_drawer.png)

   Tap the app's icon (blue microphone)

3. The following screen should appear:

   ![Main screen](demo_images/main_screen.png)

   The speech recognition will start. You can either say an address/search term,
   or say "Contact" and then the name of your contact.

4. If you say anything, the confirmation screen will appear:

   ![Confirmation screen](demo_images/confirmation_screen.png)

   On this screen you can either select the recognized text, which will open
   the address/search term in the navigation app, or you can ask for a new
   recognition if the text shown is not the desirable text.

   IF you do nothing, the navigation app will open after 10 seconds.

5. If the first word is "Contact", followed by a contact name, you will see a
   list with the matching contacts.

   ![Contacts screen](demo_images/contacts_screen.png)

   From there you can select a contact and a phone call will start.

## Developer instructions

### Software requirements

In order to build/debug the app locally, you will need the following:

- Android Studio, or other editor for android apps
- `adb` command, for connecting to your phone for debugging
- The "Desktop Head Unit (DHU)" app, for emulating the car's head unit on your
  computer. Installation instructions for DHU may be found here:
  [https://developer.android.com/training/cars/testing](https://developer.android.com/training/cars/testing)

### Steps to debug app on your computer using your phone

The steps to debug the app on your computer are the following:

1. Import the project in Android studio
2. Connect your phone to the computer via USB cable
3. Enable USB debugging on the phone through the "Developer Options" setting,
   and allow the computer to connect to the phone.
4. Enable "developer mode" in Android Auto settings
5. Start the "head unit server" from the top three dots in the Android Auto
   settings
6. Enable adb forwarding by running the command:

   ```bash
   adb forward tcp:5277 tcp:5277
   ```

7. Start the DHU on the computer, by entering to the DHU installation directory
   (e.g. `~/Android/Sdk/extras/google/auto/`)
   and running

   ```bash
   ./desktop-head-unit
   ```

   To test different resolutions you can pass options to DHU, such as

   ```bash
   ./desktop-head-unit -c config/default_1080p.in
   ```

8. Run the app from the icon in Android Studio. After the app compiles, the
   following should happen:
   - The settings activity should open in your phone, that will ask for the
     required permissions.
     From here you can also set the speech recognition language.
   - From the DHU window, select the "AA Speech To Text" app, say and address
     and watch the navigation screen opening.

### Adding translations

Currently, although all the above languages are available for the speech
recognition, so you can use the app in the listed countries, the UI is
translated in only English and Greek.

If you want the UI in your own language, you are more than welcome to submit a
pull request with a translation in your language.

To create a translation, follow the following steps:

1. In Android Studio, from the project pane (top left) select
   `automotive -> res -> values -> strings -> strings.xml`
   to open the translation file.

2. After the `strings.xml` file is open, you should see a link named
   "Open editor" on the top right corner of the main window. Click the
   "Open editor" link.

3. You will see a list with all the strings translated in English and Greek.

4. Add your language by clicking on the globe icon on the top left of the main
   window.

5. Select the language you want to add.

6. In the main window, complete the column that corresponds to your language

7. To add the language to the settings menu

   1. Open file `automotive -> res -> values -> arrays.xml`

   2. Add the relevant entries to the `display_language_entries`
      `display_language_values` string arrays. The `display_language_values`
      entry should contain only the two letter language code, not the country,
      like the existing entries.

   3. You may need to restart the app to see the new language
